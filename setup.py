import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, "README.md")) as f:
    README = f.read()


setup(
    name="rbow_demo",
    version=0.1,
    description="Demo app of websockets in Pyramid",
    long_description=README,
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    keywords="web services",
    author="Landreville",
    author_email="landreville@heyneat.ca",
    url="",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=["asgiref", "cornice", "rbow" "uvicorn", 'waitress'],
    entry_points="""\
      [paste.app_factory]
      main=rbow_demo:main
      """,
    paster_plugins=["pyramid"],
)
