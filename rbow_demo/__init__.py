from pyramid.config import Configurator


def main(global_config, **settings):
    with Configurator(settings=settings) as config:
        config.include("cornice")
        config.include("rbow")
        config.add_static_view("static", "static")
        config.add_route("web", "/web")
        config.scan("rbow_demo.views")
        return config.make_asgi_app()

# This is required for uvicorn to find the application
# Use rbow_demo:application as the spec for uvicorn.
application = main({})
