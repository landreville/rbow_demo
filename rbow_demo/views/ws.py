import json

from datetime import datetime

import logging

from rbow import (
    websocket_config,
    AsyncWebsocketConsumer,
    SyncWebsocketConsumer,
    RequestConsumer,
)

log = logging.getLogger(__name__)


@websocket_config(path="/ws-async")
class PingWebsocket(AsyncWebsocketConsumer):
    async def on_receive(self, data):
        print(f"Received: {data}")
        await self.send("Pong! (ping from WebSocket)")


@websocket_config(path="/ws-sync")
class PingSyncWebsocket(SyncWebsocketConsumer):
    def on_receive(self, data):
        print(f"Received: {data}")
        self.send("Pong! (ping from WebSocket)")


@websocket_config(path="/ws/interval")
class SubscribeToChannel(AsyncWebsocketConsumer):
    async def on_receive(self, data):
        channel_name = json.loads(data)["channelName"]

        self.channels.subscribe_to_interval(channel_name, self, 5, self.get_status)

    async def on_client_close(self):
        await super().on_client_close()
        self.channels.unsubscribe_all(self)

    async def get_status(self, send):
        await send(datetime.now().isoformat())


@websocket_config(path="/ws/ping")
class SubscribePing(AsyncWebsocketConsumer):
    async def on_receive(self, data):
        data = json.loads(data)
        channel_name = data["channelName"]
        self.channels.subscribe(channel_name, self)
        self.channels.send(channel_name, "Pong!")

    async def on_client_close(self):
        await super().on_client_close()
        self.channels.unsubscribe_all(self)


@websocket_config(path="/ws/request-ping")
class SubscribeRequestPing(RequestConsumer):
    def on_receive(self, request):
        data = request.json
        channel_name = data["channelName"]
        self.channels.subscribe(channel_name, self)
        self.channels.send(channel_name, "SubscribeRequestPing Pong!")

    def on_client_close(self):
        super().on_client_close()
        self.channels.unsubscribe_all(self)


@websocket_config(path="/ws/request-interval")
class SubscribeRequestInterval(RequestConsumer):
    def on_receive(self, request):
        data = request.json
        channel_name = data["channelName"]

        # TODO: does this really work?
        #  if the request context is over because on_receive exited
        #  (see RequestConsumer.receive) then what happens if request stuff
        #  is called that needs that context?
        async def _periodic(send):
            await send(json.dumps({"value": f"Request ID: {id(request)}"}))

        if not self.channels.has_channel(channel_name):
            self.channels.subscribe_to_interval(channel_name, self, 5, _periodic)

        return "Bleh"

    def on_client_close(self):
        super().on_client_close()
        self.channels.unsubscribe_all(self)


@websocket_config(path="/ws/request-interval")
class IdealConsumer(RequestConsumer):
    def on_receive(self, request):
        data = request.json
        entity_id = data["entity_id"]
        channel_name = f"entity|{entity_id}"

        if not self.channels.has_channel(channel_name):
            self.channels.subscribe_to_interval(
                channel_name, self, 5, PeriodicTask(entity_id)
            )

    def on_client_close(self):
        super().on_client_close()
        self.channels.unsubscribe_all(self)


class PeriodicTask(object):
    def __init__(self, entity_id):
        self._entity_id = entity_id

    def on_interval(self, request):
        status = request.db.query()
        return {"status": status}
