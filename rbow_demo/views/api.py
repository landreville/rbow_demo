import logging
from cornice import Service

log = logging.getLogger(__name__)

svc = Service(name="api ping", path="/api/ping")


@svc.post()
def ping(request):
    log.info('Received ping over HTTP.')
    request.rbow['channels'].send('test-ping-channel', 'Pong! (from HTTP ping)')
