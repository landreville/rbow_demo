from pkg_resources import resource_string
from pyramid.response import Response
from pyramid.view import view_config
from rbow import asgi_view


@view_config(route_name='web', renderer='string')
def demo(request):
    """Return the demo HTML file that loads a simple VueJS app."""
    return Response(
        resource_string('rbow_demo', 'static/demo.html'),
        content_type='text/html'
    )
