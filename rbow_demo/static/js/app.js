const app = new Vue({
  el: '#app',
  data() {
    return {
      received: [],
      sockets: {},
      statuses: []
    }
  },
  mounted() {
  },
  methods: {
    sendPing(event) {
      if (this.sockets['ws/ping']) {
        this.sockets['ws/ping'].send(
          JSON.stringify({ channelName: 'test-ping-channel', value: 'Ping!'})
        )
      }
    },
    async sendHttpPing(event) {
      await fetch('/api/ping', { method: 'POST', body: 'Ping!'})
    },
    async subscribePing() {
      const wsPath = 'ws/ping'
      const channelName = 'test-ping-channel'
      const socket = new WebSocket(`ws://localhost:6543/${wsPath}`)

      socket.addEventListener('open', (event) => {
        this.statuses.push(`${wsPath} - Open`)
        socket.send(JSON.stringify({channelName}))
      })

      socket.addEventListener('message', (event) => {
        this.received.push(`${wsPath} - ${event.data}`)
      })

      this.sockets[wsPath] = socket
    },
    unsubscribePing() {
      this.sockets['ws/ping'].close()
      this.$delete(this.sockets, 'ws/ping')
    },

    async subscribeInterval() {
      const wsPath = 'ws/interval'
      const channelName = 'test-interval-channel'
      const socket = new WebSocket(`ws://localhost:6543/${wsPath}`)

      socket.addEventListener('open', (event) => {
        this.statuses.push(`${wsPath} - Open`)
        socket.send(JSON.stringify({channelName}))
      })

      socket.addEventListener('message', (event) => {
        this.received.push(`${wsPath} - ${event.data}`)
      })

      this.sockets[wsPath] = socket
    },
    unsubscribeInterval() {
      this.sockets['ws/interval'].close()
      this.$delete(this.sockets, 'ws/interval')
    },

    sendRequestPing(event) {
      if (this.sockets['ws/request-ping']) {
        this.sockets['ws/request-ping'].send(
          JSON.stringify({ channelName: 'test-request-ping', value: 'Ping!'})
        )
      }
    },
    async subscribeRequestPing() {
      const wsPath = 'ws/request-ping'
      const channelName = 'test-request-ping'
      const socket = new WebSocket(`ws://localhost:6543/${wsPath}`)

      socket.addEventListener('open', (event) => {
        this.statuses.push(`${wsPath} - Open`)
        socket.send(JSON.stringify({channelName}))
      })

      socket.addEventListener('message', (event) => {
        this.received.push(`${wsPath} - ${event.data}`)
      })

      this.sockets[wsPath] = socket
    },
    unsubscribeRequestPing() {
      this.sockets['ws/request-ping'].close()
      this.$delete(this.sockets, 'ws/request-ping')
    },

    async subscribeRequestInterval() {
      const wsPath = 'ws/request-interval'
      const channelName = 'test-request-interval-channel'
      const socket = new WebSocket(`ws://localhost:6543/${wsPath}`)

      socket.addEventListener('open', (event) => {
        this.statuses.push(`${wsPath} - Open`)
        socket.send(JSON.stringify({channelName}))
      })

      socket.addEventListener('message', (event) => {
        this.received.push(`${wsPath} - ${event.data}`)
      })

      this.sockets[wsPath] = socket
    },
    unsubscribeRequestInterval() {
      this.sockets['ws/request-interval'].close()
      this.$delete(this.sockets, 'ws/request-interval')
    },
  }
});
